const { model, Schema } = require("mongoose");

const userSchema = new Schema({
  login: {
    type: String,
    unique: true, //вызывает (node:3244) DeprecationWarning: collection.ensureIndex is deprecated. Use createIndexes instead.
    required: true
  },
  password: {
    type: String,
    required: true,
    minLength: 6
  }
});

module.exports = model("users", userSchema);
