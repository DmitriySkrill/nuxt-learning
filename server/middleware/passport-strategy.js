const { Strategy, ExtractJwt } = require("passport-jwt");
//const { model } = require("mongoose");
const keys = require("../keys");
//const User = model("users");
const User = require('../models/user.model')

// Autherization: Bearer dsfsdfsdf.sdfsdfsdf.sdfsf
const options = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: keys.JWT
};

module.exports = new Strategy(options, async (payload, done) => {
  try {
    // В payload хранится то, что записываем в jwt.sign (auth.controller.js)
    const candidate = await User.findById(payload.userId).select("id");
    if (candidate) {
      done(null, candidate);
    } else {
      done(null, false);
    }
  } catch (error) {
    console.error(error);
  }
});
